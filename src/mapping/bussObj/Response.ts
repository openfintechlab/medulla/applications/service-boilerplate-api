/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Wrapper Module for Generating Posting message generic response message
 * Please refer to project-docs/registiration/md-rb-001-resp.jsonc for mode details
 */
export namespace PostRespBusinessObjects {
    interface IGenericResponse {
        metadata: IMetadata;
    }

    interface IMetadata {
        status: string;
        description: string;
        responseTime: Date;
        trace: ITrace[];
    }

    interface ITrace {
        source: string;
        description: string;
    }

    /**
     * Wrapping all trace elements
     */
    export class Trace implements ITrace {
        source: string;
        description: string;

        constructor(source?: string, description?: string) {
            this.source = source || "";
            this.description = description || "";
        }
    }

    /**
     * Wrapper class for Metadata
     */
    export class Metadata implements IMetadata {
        status!: string;
        description!: string;
        responseTime!: Date;
        trace!: Trace[];
    }


    /**
     * Class for generating Posting Response
     */
    export class PostingResponse implements IGenericResponse {
        metadata: Metadata;

        constructor() {
            this.metadata = new Metadata();
        }

        /**
         * Generates a generic PostingResponse with status and description
         * @param status 4 characters status code
         * @param decsription  Description of the error message
         */
        public generateBasicResponse(status: string, decsription: string, ...trace: Trace[]): string {
            this.metadata.status = status;
            this.metadata.description = decsription;            
            if(trace.length > 0){
                this.metadata.trace = trace;
            }
            return JSON.stringify(this);
        }        

        get metadataRoot(): Metadata{
            return this.metadata;
        }

    }

}