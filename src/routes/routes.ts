/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Root enry level file forr bootstarting node js application
 */

import express from "express";
import {AppHealth}    from "../config/AppConfig";
import logger from "../utils/Logger";


const router: any = express.Router();


/**
 * Routes Definition for health, readiness and liveness check
 */
 // Route for liveness prone
 // The kubelet kills the container and restarts it.
 router.get('/healthz',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    if(AppHealth.config_loaded && !AppHealth.reload_required)
        res.status(200);    
    else{
        logger.error(`Health check fail. Config Loaded: ${AppHealth.config_loaded} and Express Loaded: ${AppHealth.express_loaded}`);
        res.status(503);    
    }
        
    res.send();
});


// Route for rediness check. 
// This route will return 200 in-case all required bootstarap is finished
// Note: We want to suspend traffic in-case there is something wrong here
router.get('/readiness',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    if(AppHealth.config_loaded)
        res.status(200);    
    else{
        logger.error(`Readiness check fail. Config Loaded: ${AppHealth.config_loaded} and Express Loaded: ${AppHealth.express_loaded}`);
        res.status(503);        
    }        
    res.send();
});


// Protect slow starting containers with startup probes
router.get('/startup',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    if(AppHealth.config_loaded && AppHealth.express_loaded){
        res.status(200);    
    }else{
        logger.error(`Startup check fail. Config Loaded: ${AppHealth.config_loaded} and Express Loaded: ${AppHealth.express_loaded}`);        
        res.status(503);    
    }        
    res.send();
});


export default router;